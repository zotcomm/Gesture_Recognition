from modules.detector.op_ssd_detector import OP_SSD_Detector
from modules.detector.op_detector import OP_Detector
import cv2
import time
from modules.utils.utils import *
#from modules.predictor.predictor import Predictor
import imageio
import os
import numpy as np
import argparse
import modules.utils.settings as settings

def parse_args():
    parser = argparse.ArgumentParser(description='Detector test unit')
    parser.add_argument('--op_only', help='Use openpose only detector', action="store_true")
    parser.add_argument('--input_path', metavar='input_path', required=True,
                        help='Path to the video to use as input', type=str, default=None)
    parser.add_argument('--output_path', metavar='output_path',
                        help='Path to output file', type=str, default=None,)
    args = parser.parse_args()
        
    return args

def main():

    args = parse_args()
    print('-----------------Settings-----------------')
    for arg in vars(args):
        print(arg, ': ', getattr(args, arg))
    print('------------------------------------------')

    if args.op_only:
        print('OP only detector')
        det = OP_Detector()
    else:
        print('OP_SSD detector')
        det = OP_SSD_Detector()

    classes = sorted(settings.classes)  #ensure the classes are sorted
    pred = Predictor(model_path = settings.model_path)
    
    if not is_image(args.input_path):
      print('Invalid file format, only .jpg and .png images are supported.')
      return
        
    print('Reading image file',args.input_path)
    t0 = time.time()
    image = cv2.imread(args.input_path)
    if image is None:
      print('Error:',args.input_path,'was not found or may be corrupted.')
      return

    image = image[:, :, :3]  # BGR
    print('Image read successfully.')
    t1 = time.time()
    image, rectangles, handKeypoints= det.detect_hands(image, draw_enable = 1)
    t2 = time.time()
    print('The time for detection on the image : ', t2-t1, ' [sec]')
       
    black_frame = np.zeros( image.shape, dtype = np.uint8 )
    draw_hand_keypoints(black_frame, rectangles, handKeypoints)
        
    predictions = []
    for i in range(len(rectangles)):  #For each rectangle pair (person)
        for j in range(len(rectangles[i])):  #Left hand then right hand
            if check_data(rectangles[i][j], handKeypoints[j][i]):  #Sanity check
                x = int(rectangles[i][j].x)
                y = int(rectangles[i][j].y)
                w = int(rectangles[i][j].width)
                h = int(rectangles[i][j].height)
                cropped_frame = black_frame[max(y, 0):y+h, max(x, 0):x+h]
                cropped_frame = cv2.resize(cropped_frame,(256,256))
                prediction, score = pred.predict_image(cropped_frame)
                predictions.append((classes[prediction], "{0:.2f}".format(score)))
        
                  
    t3 = time.time()
    print('The time for prediction on the image : ', t3-t2, ' [sec]')
    print('Predictions on the image are : ', predictions)
    
    draw_str(image, (20, 40), "Predictions : {}".format(predictions))
    draw_str(image, (20, 60), "Time to get: {:.3f} sec".format(t1-t0))
    draw_str(image, (20, 80), "Time to detect: {:.3f} sec".format(t2-t1))
    draw_str(image, (20, 100), "Time to predict: {:.3f} sec".format(t3-t2))
    
    if args.output_path:
      if is_image(args.output_path):
        if not os.path.dirname(args.output_path) == '':
          os.makedirs(os.path.dirname(args.output_path), exist_ok=True)
        print('Saving output to', args.output_path)
        cv2.imwrite(args.output_path, image)
      else:
        print('Error: Output must be a .jpg or .png file')
        return
    else:
        print('Only showing output image using opencv. Please enter an output path if you would like to save it in a file.')
    
    cv2.imshow('output', image)
    
    while(True):
       k = cv2.waitKey(1)
       if k == 27 or cv2.getWindowProperty('output',cv2.WND_PROP_VISIBLE) < 1 : #wait for escape key
           cv2.destroyAllWindows()
           break

if __name__ == '__main__':
    main()