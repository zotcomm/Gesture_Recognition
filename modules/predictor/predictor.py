import torch
from torchvision import transforms
from torch.autograd import Variable
from PIL import Image
import os
from modules.utils.settings import  sample_size

class Predictor():
  def __init__(self, model_path):
    if not os.path.isfile(model_path):
      raise Exception('Error: File '+model_path+' was not found.')

    self.trained_model = torch.load(model_path)
    self.trained_model.eval()
    self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

  def predict_image(self, frame):
    image = Image.fromarray(frame.squeeze())
    test_transforms = transforms.Compose([transforms.Resize(sample_size),
                                      transforms.ToTensor(),
                                      transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                                     ])
    
    image_tensor = test_transforms(image).float()
    image_tensor = image_tensor.unsqueeze_(0)
    input = Variable(image_tensor)
    input = input.to(self.device)
    output = self.trained_model(input)
    
    score, prediction = torch.max(output, dim=1)
        
    return (prediction.item(), score.item())         #torch.argmax(output).item()