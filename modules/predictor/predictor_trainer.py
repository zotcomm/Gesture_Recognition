import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torchvision import datasets, models, transforms
import time
import os
import copy
from enum import Enum
from warnings import warn
from modules.utils.settings import  sample_size

class ModelType(Enum):
  VGG = 1
  RESNET = 2

class PredictorTrainer():
  def __init__(self, model_type):
    self.model_type = model_type
    self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    if model_type == ModelType.VGG:
      self.model = models.vgg16(pretrained=True)
    elif model_type == ModelType.RESNET:
      self.model = models.resnet18(pretrained=True)
    else:
      raise Exception('Error: Invalid model type. Please use ModelType.VGG or ModelType.RESNET.')

    self.dataloaders = None
    self.dataset_sizes = None
    self.class_names = None

  def __compile(self):
    self.criterion = nn.CrossEntropyLoss()
    self.optimizer= optim.SGD(self.model.parameters(), lr=0.001, momentum=0.9)
    self.scheduler = lr_scheduler.StepLR(self.optimizer, step_size=7, gamma=0.1)

  # load the dataset from folders
  def load_dataset(self, data_dir, batch_size):
    print('Loading dataset ...')
    data_transforms = {
    'train': transforms.Compose([
        transforms.Resize(sample_size),
        transforms.RandomHorizontalFlip(),
        transforms.RandomRotation(degrees = (-25,25) ),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
    'valid': transforms.Compose([
        transforms.Resize(sample_size),
        transforms.RandomHorizontalFlip(),
        transforms.RandomRotation(degrees = (-25,25) ),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ]),
    'test': transforms.Compose([
        transforms.Resize(sample_size),
        transforms.RandomHorizontalFlip(),
        transforms.RandomRotation(degrees = (-25,25) ),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])
    }
    
    for folder in ['train', 'valid', 'test']:
      if not os.path.isdir( os.path.join(data_dir, folder)):
        raise Exception('Error: Dataset directory is invalid. Please ensure it contains train, valid and test folders.')

    image_datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x), data_transforms[x])
                    for x in ['train', 'valid', 'test']}
  
    self.dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size = batch_size, shuffle=True, num_workers=4)
                  for x in ['train', 'valid', 'test']}
    self.dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'valid', 'test']}
    
    if image_datasets['train'].classes != self.class_names:
      if self.class_names is not None:
        warn('Changes to the dataset classes resulted in changes to the model.')
       
      self.class_names = image_datasets['train'].classes

      if self.model_type == ModelType.VGG:
        self.model.classifier[-1] = nn.Linear(in_features=4096, out_features=len(self.class_names))
        self.model = self.model.to(self.device)
      elif self.model_type == ModelType.RESNET:
        num_ftrs = self.model.fc.in_features
        self.model.fc = nn.Linear(num_ftrs, len(self.class_names))
        self.model = self.model.to(self.device)

    print('Dataset loaded.')
    print('Classes:',self.class_names)

  # train the model using either VGG16 or RESNET
  def train_model(self, num_epochs):
    if self.dataloaders is None:
      raise Exception('Error: You must load a dataset before training the model. Please use load_dataset().')

    print('Compiling model ...')
    self.__compile()
    print('Model compiled.')

    print('Started training ...')
    since = time.time()
    best_model_wts = copy.deepcopy(self.model.state_dict())
    best_acc = 0.0

    for epoch in range(num_epochs):
      print("Epoch ", epoch+1, "/", num_epochs)
      print('-' * 10)

      # Each epoch has a training and validation phase
      for phase in ['train', 'valid']:
        if phase == 'train':
          self.model.train()  # Set model to training mode
        else:
          self.model.eval()   # Set model to evaluate mode

        running_loss = 0.0
        running_corrects = 0

        # Iterate over data.
        for inputs, labels in self.dataloaders[phase]:

          inputs = inputs.to(self.device)
          labels = labels.to(self.device)

          # zero the parameter gradients
          self.optimizer.zero_grad()

          # print(type(inputs))
          # forward
          # track history if only in train
          with torch.set_grad_enabled(phase == 'train'):
            outputs = self.model(inputs)
            _, preds = torch.max(outputs, 1)
            loss = self.criterion(outputs, labels)

            # backward + optimize only if in training phase
            if phase == 'train':
              loss.backward()
              self.optimizer.step()

          running_loss += loss.item() * inputs.size(0)
          running_corrects += torch.sum(preds == labels.data)

        if phase == 'train':
          self.scheduler.step()

        epoch_loss = running_loss / self.dataset_sizes[phase]
        epoch_acc = running_corrects.double() / self.dataset_sizes[phase]

        print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))

            # deep copy the model
        if phase == 'valid' and epoch_acc > best_acc:
          best_acc = epoch_acc
          best_model_wts = copy.deepcopy(self.model.state_dict())

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))
    self.model.load_state_dict(best_model_wts)  

  def save_model(self, model_file_name):
    if not os.path.dirname(model_file_name) == '':
      os.makedirs(os.path.dirname(model_file_name), exist_ok=True)
      
    #torch.save(self.model.state_dict(), model_file_name)     # save the parameters of trained model
    torch.save(self.model, model_file_name)                  # or save the entire model

  #Evaluates the model on the (labeled) testing data set and returns its accuracy (0-100).
  def evaluate_model(self, verbose = 0):
    if(verbose > 0):
      print('Evaluating model ...')
    
    if self.dataloaders is None:
      raise Exception('Error: You must load a dataset before evaluating the model. Please use load_dataset().')

    was_training = self.model.training
    self.model.eval()
    running_corrects = 0

    with torch.no_grad():
        for i, (inputs, labels) in enumerate(self.dataloaders['test']):
            inputs = inputs.to(self.device)
            labels = labels.to(self.device)
            outputs = self.model(inputs)
            _, preds = torch.max(outputs, 1)
              
            correct_this_batch = 0
            for j in range(inputs.size()[0]):
                if( preds[j] == labels[j]):
                  correct_this_batch += 1
            running_corrects += correct_this_batch
            if(verbose > 1):
              print('Batch',i,':',correct_this_batch,'/',inputs.size()[0],'correct.')
          
        accuracy = running_corrects/self.dataset_sizes['test'] * 100
        if(verbose > 0):
          print('Evaluation ended.')
          print('Total correct: {}/{} ({:.4}%)'.format(
              running_corrects, self.dataset_sizes['test'], accuracy) )
        self.model.train(mode=was_training)
        return accuracy