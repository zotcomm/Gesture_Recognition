from abc import ABC, abstractmethod

class Detector( ABC):
		
	@abstractmethod
	def detect_hands(self, frame, draw_enable): 
		pass