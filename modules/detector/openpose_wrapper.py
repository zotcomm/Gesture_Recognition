import math
import numpy as np
import cv2

from frameworks.openpose.build.python.openpose import pyopenpose as op
from modules.utils.settings import model_folder, render_pose, render_pose, net_resolution, hand_net_resolution

class OpenposeWrapper():

	def __init__(self, detect_hand, detect_body, hand_detection_mode = None):
		# Starting OpenPose
		params = dict()
		params["model_folder"] = model_folder
		params["render_pose"] = render_pose
		params["net_resolution"] = net_resolution
		params["hand_net_resolution"] = hand_net_resolution
		params["camera"] = 0


		params["hand"] = detect_hand
		if hand_detection_mode is not None:
		  params["hand_detector"] = hand_detection_mode
		params["body"] = detect_body
		
		self.op_wrapper = op.WrapperPython()
		self.op_wrapper.configure(params)
		self.op_wrapper.start()
		
	def predict(self, datum):
		self.op_wrapper.emplaceAndPop([datum])
		return datum
	
	# Performs cleanup for the given framework
	def cleanup(self):
		pass
