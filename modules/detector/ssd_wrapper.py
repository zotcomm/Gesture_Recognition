#import tensorflow as tf
import numpy as np
import cv2
from frameworks.ssd.utils import detector_utils as detector_utils

# Testing
import os
import time
import imageio
from modules.utils.settings import ssd_frame_size, num_hands, score_thresh, scaling_factor

class SSDWrapper():

    def __init__(self):
        self.detection_graph, self.sess = detector_utils.load_inference_graph()

    # Returns coordinates of a bounding box around the hand of the user.
    # [p1,p2] representing the cooridnates of the top left point  and bottom
    # right point of the box. Returns None if no hand detected
    def detect_bboxes(self, frame):
        original_height, original_width = frame.shape[:2]
        new_w, new_h = ssd_frame_size
        x_ratio = original_width/new_w
        y_ratio = original_height/new_h

        resized_frame = cv2.resize(frame, (new_w, new_h))  # width, height
        # since input frames are given in BGR format, we must convert them to
        # RGB
        resized_frame = cv2.cvtColor(resized_frame, cv2.COLOR_BGR2RGB)

        boxes, scores = detector_utils.detect_objects(
            resized_frame, self.detection_graph, self.sess)
        valid_boxes = []
        valid_boxes_scores = []
        for i in range(num_hands):
            if(scores[i] > score_thresh):
                x1, x2, y1, y2 = (boxes[i][1] *
                                  new_w, boxes[i][3] *
                                  new_w, boxes[i][0] *
                                  new_h, boxes[i][2] *
                                  new_h)
                p1 = (int(x1*x_ratio), int(y1*y_ratio))
                p2 = (int(x2*x_ratio), int(y2*y_ratio))
                assert(x2 - x1 >= 0)
                assert(y2 - y1 >= 0)
                valid_boxes.append([x1*x_ratio, y1*y_ratio, (x2 - x1)*x_ratio, (y2 - y1)*y_ratio])  # x, y, w, h
                valid_boxes_scores.append(scores[i])

                for i in range(len(valid_boxes)):
                    x,y,w,h = valid_boxes[i]
                    #scaling
                    valid_boxes[i][0] = x - (scaling_factor - 1)/2 * w
                    valid_boxes[i][1] = y - (scaling_factor - 1)/2 * h
                    valid_boxes[i][2] = scaling_factor * w
                    valid_boxes[i][3] = scaling_factor * h

        return frame, valid_boxes, valid_boxes_scores

    # Performs cleanup for the given framework
    def cleanup(self):
        pass
