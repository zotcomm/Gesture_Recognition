import cv2
from modules.detector.openpose_wrapper import OpenposeWrapper
from modules.detector.ssd_wrapper import SSDWrapper
from frameworks.openpose.build.python.openpose import pyopenpose as op
from modules.detector.detector import Detector
from modules.utils.utils import draw_hand_keypoints 
import modules.utils.settings as settings

class OP_SSD_Detector(Detector):

    # Constructor
    def __init__(self):
        
        self.ssd_wrapper = SSDWrapper()
        self.openpose_hand_wrapper = OpenposeWrapper(detect_hand = True, detect_body = 0, hand_detection_mode = 2)

    def detect_hands(self, frame, draw_enable = 2):
        #Returns the frame, the box coordinates detected by ssd (up to num_hands box that match the score threshold).
        #We need to figure out if each box is a right or a left hand
        
        frame, boxes, scores = self.ssd_wrapper.detect_bboxes(frame)

    #Construct the hand rectangles for keypoint detection. Each hand box will be treated as if it belongs to a different person since it doesn't matter which person executes a gesture
    #Rectangles must be squared for openpose to detect the hand keypoints
        handRectangles = []
        for i in range(len(boxes)):
            dim = max(boxes[i][2], boxes[i][3])
            
            if settings.ssd_mode == settings.SSD_Mode.RIGHTONLY:
                handRectangles.append([op.Rectangle(0,0,0,0), op.Rectangle(boxes[i][0], boxes[i][1], dim, dim)])
            elif settings.ssd_mode == settings.SSD_Mode.LEFTONLY:
                handRectangles.append([op.Rectangle(boxes[i][0], boxes[i][1], dim, dim), op.Rectangle(0,0,0,0)])
            elif settings.ssd_mode == settings.SSD_Mode.MULTIHAND:
                handRectangles.append([op.Rectangle(boxes[i][0], boxes[i][1], dim, dim), op.Rectangle(boxes[i][0], boxes[i][1], dim, dim)])
            else:
                print("Error:Invalid detection mode for op_ssd_detector")
                return frame, [], []

        #print('handRectangles are ', handRectangles)
        hand_datum = op.Datum()
        hand_datum.cvInputData = frame
        hand_datum.handRectangles = handRectangles
        hand_datum = self.openpose_hand_wrapper.predict(hand_datum)
        
        handKeypoints = hand_datum.handKeypoints
        rectangles = hand_datum.handRectangles
        
        #Label each SSD box
        box_labels = []

        if handKeypoints != [] and rectangles != []:
          assert( len(handKeypoints) == 2)
          assert(handKeypoints[0].size >= 63 and handKeypoints[1].size >= 63)
          assert( len( handKeypoints[0] ) == len(handKeypoints[1]) )
          num_people = len( handKeypoints[0] )
          
          for i in range( num_people ):

            conf_sum_left = 0
            conf_sum_right = 0
            
            for point in range(21):
                conf_sum_left += handKeypoints[0][i][point][2]
                conf_sum_right += handKeypoints[1][i][point][2]
            
            if conf_sum_left > conf_sum_right:
                box_labels.append('l')
            elif conf_sum_left < conf_sum_right:
                box_labels.append('r')
            else:
                center_x = rectangles[i][1].x + rectangles[i][1].width/2 #same rectangle left and right
                center_y = rectangles[i][1].y + rectangles[i][1].height/2

                _ ,frame_width = frame.shape[:2]
                if (center_x > frame_width /2):
                    box_labels.append('l')
                else:
                    box_labels.append('r')

          assert(num_people == len(box_labels))

          for i in range( len(box_labels) ):
              if box_labels[i] == 'l':    #discard right
                rectangles[i][1] = op.Rectangle(0, 0, 0, 0)
                handKeypoints[1][i] = [ [0.0, 0.0, 0.0] for _ in range(21)]
              else:    #discard left
                rectangles[i][0] = op.Rectangle(0, 0, 0, 0)
                handKeypoints[0][i] = [ [0.0, 0.0, 0.0] for _ in range(21)]
        
        if draw_enable >= 1:
            #frame = hand_datum.cvOutputData
            draw_hand_keypoints(frame, rectangles, handKeypoints)
            
        if draw_enable >= 2:
            for i in range(len(boxes)):
                p1 = (int(boxes[i][0]), int(boxes[i][1]))
                dim = max(boxes[i][2],boxes[i][3])
                p2 = (int(boxes[i][0] + dim), int(boxes[i][1] + dim))
                color = settings.left_hand_box_color if box_labels[i] == 'l' else settings.right_hand_box_color
                cv2.rectangle(frame, p1, p2, color, 2)

        #print('hand keypoints are ', hand_datum.handKeypoints)
        return frame, rectangles, handKeypoints
    
    # Destructor
    def __del__(self): 
        self.ssd_wrapper.cleanup()
        self.openpose_hand_wrapper.cleanup()
        #self.openpose_body_wrapper.cleanup()
        print("Framework wrapper has cleaned up")
        
