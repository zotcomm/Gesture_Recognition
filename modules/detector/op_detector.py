import cv2
from modules.detector.openpose_wrapper import OpenposeWrapper
from frameworks.openpose.build.python.openpose import pyopenpose as op
from modules.detector.detector import Detector
from modules.utils.utils import draw_hand_keypoints 
import modules.utils.settings as settings

class OP_Detector(Detector):

	# Constructor
	def __init__(self):
		self.op_wrapper = OpenposeWrapper(detect_hand = True, detect_body = 1, hand_detection_mode = 0)

	def detect_hands(self, frame, draw_enable = 2):
		datum = op.Datum()
		datum.cvInputData = frame
		self.op_wrapper.predict(datum)
		
		if len(datum.handRectangles) == 0 or datum.poseKeypoints.size < 75:
		  return frame, [], []

		assert(len(datum.poseKeypoints) == len(datum.handRectangles))

		if draw_enable >= 1:
		  #frame = datum.cvOutputData
		  draw_hand_keypoints(frame, datum.handRectangles, datum.handKeypoints)
		
		if draw_enable >= 2:
		  for i in range(len(datum.handRectangles)): #draw the rectangles
		    #print('person ', i, 'hand rectangles : ', datum.handRectangles[i])
		    left_rect = datum.handRectangles[i][0]
		    right_rect = datum.handRectangles[i][1]

		    p1 = (int(left_rect.x), int(left_rect.y))
		    p2 = (int(left_rect.x + left_rect.width), int(left_rect.y + left_rect.height))
		    cv2.rectangle(frame, p1, p2, settings.left_hand_box_color, 2)
            
		    p3 = (int(right_rect.x), int(right_rect.y))
		    p4 = (int(right_rect.x + right_rect.width), int(right_rect.y + right_rect.height))
		    cv2.rectangle(frame, p3, p4, settings.right_hand_box_color, 2)
        
		return frame, datum.handRectangles, datum.handKeypoints

	# Destructor
	def __del__(self):
		self.op_wrapper.cleanup()
		print("Framework wrapper has cleaned up")
		
