import os
import cv2
import math
from modules.predictor.predictor import Predictor
from modules.utils.settings import hand_render_threshold, detection_count_threshold

def write_video(cap, write_path):
  w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
  h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

   #Note: these are plateform dependent
  codecs = { 'mp4v': ['.mp4', '.MP4', '.mov', '.MOV'],
             'XVID': ['.avi', '.AVI']}
  codec = 'mp4v' #default
  for key, value in codecs.items():
    if write_path[-4:] in value:
      codec = key
      break

  fourcc = cv2.VideoWriter_fourcc(*codec)
  out = cv2.VideoWriter(write_path, fourcc, 20, (w, h), True)
  return out
	
def is_video(filename):
    videoformats = ['.mp4', '.MP4', '.mov', '.MOV', '.avi', '.AVI']
    return filename[-4:] in videoformats
      
def is_image(filename):
    imageformats = ['.jpg', '.JPG', '.png', '.PNG', 'jpeg', 'JPEG']
    return filename[-4:] in imageformats

def create_directory(output_path):
    
    if not os.path.exists(output_path):
        os.makedirs(output_path)

def check_data( rectangle , handKeypoints):     #sanity check for prediction
  if( rectangle.x + rectangle.y + rectangle.width + rectangle.height <= 0 ):
    return False

  detection_count = 0
  for i in range(21):
    if handKeypoints[i][2] >= hand_render_threshold:
      detection_count+=1
  return detection_count >= detection_count_threshold

def draw_str(dst, target, s):
  x, y = target
  cv2.putText(dst, s, (x+1, y+1), cv2.FONT_HERSHEY_PLAIN, 1.0, (0, 0, 0), thickness = 2, lineType=cv2.LINE_AA)
  cv2.putText(dst, s, (x, y), cv2.FONT_HERSHEY_PLAIN, 1.0, (255, 255, 255), lineType=cv2.LINE_AA)


def __draw_hand(frame, rectangle ,keypoints):
    assert( len(keypoints) == 21 )
    assert( rectangle.width == rectangle.height )

    thresh = 0.2
    thickness_circle_ratio = 1.0/50.0
    thickness_line_ratio_wrt_circle = 0.75

    frame_width , frame_height , _ = frame.shape
    frame_area = frame_width * frame_height

    #thickness scaling
    areas_ratio = min(1 , max( rectangle.width / frame_width , rectangle.height / frame_height) )
    thickness_ratio = max(2 , int(round( math.sqrt(frame_area) * thickness_circle_ratio * areas_ratio)) )
    #thickness_circle = max( 1, thickness_ratio if areas_ratio > 0.05 else -1 )
    thickness_line = max(1, int(round(thickness_ratio * thickness_line_ratio_wrt_circle)) )
    circle_radius = thickness_ratio/2

    edge_colors = [(0,0,105), (0,105,105), (0,105,0), (105,105,0), (105,0,105)]
    for i in range(21):
        
        if i%4 == 1 and keypoints[i][2] > thresh and keypoints[0][2] > thresh:          #1-0, 5-0, 9-0, 13-0, 17-0
            cv2.line(frame, tuple(keypoints[i][:2]), tuple(keypoints[0][:2]), edge_colors[0], thickness= int(thickness_line), lineType=8, shift = 0)

        if i%4 !=0 and keypoints[i][2] > thresh and keypoints[i+1][2] > thresh:          #other edges
            edge_colors[0] = tuple(map(lambda i : i + 50 if i+50 > 0 else i, edge_colors[0] ))
            cv2.line(frame, tuple(keypoints[i][:2]), tuple(keypoints[i+1][:2]), edge_colors[0], thickness= int(thickness_line), lineType=8, shift = 0)
        
        if keypoints[i][2] > thresh:    #draw circles
            cv2.circle(frame, tuple(keypoints[i][:2]), int(circle_radius), (255,0,0 ))
        
        if i%4 == 3:
            edge_colors.pop(0)
       
    
def draw_hand_keypoints(frame, rectangles, handKeypoints):

    if handKeypoints != [] and rectangles != []:
          assert( len(handKeypoints) == 2)
          assert( len( handKeypoints[0] ) == len(handKeypoints[1]) )
          num_people = len( handKeypoints[0] )
          
          for i in range( num_people ):
            __draw_hand  ( frame, rectangles[i][0], handKeypoints[0][i])
            __draw_hand  ( frame, rectangles[i][1], handKeypoints[1][i])

