from enum import Enum


#General
hand_render_threshold = 0.2
detection_count_threshold = 5
right_hand_box_color = (255, 255, 0) #cyan
left_hand_box_color = (0, 255, 255) #yellow

#Training
sample_size = (368,368)

#Openpose settings
model_folder= 'frameworks/openpose/models/'
render_pose = 0                                                         #disable openpose rendering
net_resolution = '-1x80'                                              #defaut 368x368
hand_net_resolution = '368x368'                               #defaut 368x368

#SSD settings
class SSD_Mode(Enum):
  LEFTONLY = 1
  RIGHTONLY = 2
  MULTIHAND = 3

num_hands = 1
score_thresh = 0.1
ssd_frame_size = (320,180)
scaling_factor = 1.6
ssd_mode = SSD_Mode.RIGHTONLY
#Predictor settings
classes =  ['fist', 'palm', 'peace', 'rock-on', 'sideways', 'thumbs-up']                        #default
prediction_thresholds =   [0,0,0,0,0,0]                       #default
model_path =   'models/trained_model.pt'       #default