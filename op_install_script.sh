#!/bin/bash
install_location="./frameworks"

echo 'Preparing directory ...'
mkdir -p $install_location
rm -rf $install_location/openpose

echo 'Cloning github repository ...'
git clone -q --depth 1 https://github.com/CMU-Perceptual-Computing-Lab/openpose.git $install_location/openpose

echo 'Downloading cmake ...'
rm -rf $install_location/openpose/cmake-3.13.0-Linux-x86_64.tar.gz
wget -q https://cmake.org/files/v3.13/cmake-3.13.0-Linux-x86_64.tar.gz -P $install_location/openpose
tar xfz $install_location/openpose/cmake-3.13.0-Linux-x86_64.tar.gz --strip-components=1 -C /usr/local

echo 'Modifying CMakeLists.txt ...'

sed -i 's/COMMAND git checkout master/COMMAND git checkout f019d0dfe86f49d1140961f8c7dec22130c83154/g' $install_location/openpose/CMakeLists.txt
sed -i 's/DBUILD_python=OFF/DBUILD_python=ON/g' $install_location/openpose/CMakeLists.txt
sed -i 's/option(BUILD_PYTHON "Build OpenPose python." OFF)/option(BUILD_PYTHON "Build OpenPose python." ON)/g' $install_location/openpose/CMakeLists.txt

#sed -i 's/option(USE_CUDNN "Build OpenPose with cuDNN library support." ON)/option(USE_CUDNN "Build OpenPose with cuDNN library support." OFF)/g' $install_location/openpose/CMakeLists.txt


echo 'Creating build directory ...'
rm -rf $install_location/openpose/build 
mkdir $install_location/openpose/build

echo 'Installing required dependencies ...'
apt-get update
apt-get -qq install -y libatlas-base-dev libprotobuf-dev libleveldb-dev libsnappy-dev libhdf5-serial-dev protobuf-compiler libgflags-dev libgoogle-glog-dev liblmdb-dev opencl-headers ocl-icd-opencl-dev libviennacl-dev doxygen

echo 'Creating cmake configuration ...'
cwd=$(pwd)
cd $install_location/openpose/build && cmake ..
cd $cwd

echo 'Building openpose ...'
make -j`nproc` -C$install_location/openpose/build
