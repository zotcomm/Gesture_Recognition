import os
import os.path
import time
import random
from modules.detector.op_ssd_detector import OP_SSD_Detector
from modules.detector.op_detector import OP_Detector
import cv2
from modules.utils.utils import check_data, draw_hand_keypoints, is_video
import imageio
import argparse
import csv
import numpy as np

done = 0

def parse_args():
    parser = argparse.ArgumentParser(description='Detector test unit')
    parser.add_argument('--input_path', metavar='input_path',
                        help='Path to the video to use as input', required=True, type=str)                        
    parser.add_argument('--output_path', metavar='output_path', required=True, type=str,
                        help='Path to output file')
    parser.add_argument('--gesture_name', metavar='gesture_name', required=True,
                        help='type of hand to write in a file', type=str)
    parser.add_argument('--max_samples', metavar='max_samples',
                        help='Limit on how many hand samples are extracted', type=int)
    args = parser.parse_args()

    return args

#Can improve using min heap
def placer( categories, ratios ):
  assert(len(ratios) == len(categories))
  global done
  
  total_stored_this_far = 0
  placement_volumes = dict.fromkeys(categories, 0)
  index = 0
  while not done:
    rand = random.randint(0, len(categories)-1)
    while( total_stored_this_far * ratios[rand] < placement_volumes[categories[rand]]):
      rand = (rand+1)%len(categories)
    placement_volumes[categories[rand]] += 1
    total_stored_this_far += 1
    yield categories[rand]
 
  yield placement_volumes #Final yield

def dataset_generator(video_path, output_path, gesture_name, directory_placer, max_samples):
  global done
  det = OP_Detector()
  cap = cv2.VideoCapture(video_path)

  naming_disp = {
      "train" : len(os.listdir(output_path+'/train/'+gesture_name)),
      "test"  : len(os.listdir(output_path+'/test/'+gesture_name)),
      "valid" : len(os.listdir(output_path+'/valid/'+gesture_name))
  }
  total_hands = 0
  frame_num = 0

  while cap.isOpened():
    ret, frame = cap.read()
  
    if frame is None:
      break
  
    frame_num += 1

    print('Processing frame ',frame_num,':')

    _frame, rectangles, handKeypoints = det.detect_hands(frame, draw_enable = 0)

    frame = np.zeros( frame.shape, dtype = np.uint8 )
    draw_hand_keypoints(frame, rectangles, handKeypoints)

    for i in range(len(rectangles)):  #For each rectangle pair (person)
      for j in range(len(rectangles[i])):  #Left hand then right hand
        if max_samples is not None and total_hands >= max_samples:  #No need to save any more samples
          done = 1;
          return;
        elif check_data(rectangles[i][j], handKeypoints[j][i]):  #Sanity check
          x = int(rectangles[i][j].x)
          y = int(rectangles[i][j].y)
          w = int(rectangles[i][j].width)
          h = int(rectangles[i][j].height)
          cropped_frame = frame[max(y, 0):y+h, max(x, 0):x+h]
          cropped_frame = cv2.resize(cropped_frame,(256,256))

          placement = next(directory_placer)
          image_name = gesture_name + '_' + str(naming_disp[placement]) + '.jpg'
          image_path = output_path + '/' + placement + '/' + gesture_name + '/' + image_name

          print('Placed ',image_name,' in ',placement, '. Full path: ',image_path)
          naming_disp[placement] += 1
          cv2.imwrite( image_path , cropped_frame)

          csv_filepath = output_path + '/'+ placement + '.csv'
          print('Logging ',image_name,' data to', csv_filepath)
          log_to_csv(csv_filepath, image_name, handKeypoints[j][i], j, gesture_name)
          
          total_hands += 1
  if max_samples is not None:
    print('Warning: Unable to extract ',max_samples,' samples. Your video may be too short.')
  done = 1

def log_to_csv(csv_filepath, image_name, handKeypoints, is_right_hand, gesture_name):
  assert(is_right_hand == 0 or is_right_hand == 1)

  f1 = open(csv_filepath, 'a')
  comma = csv.writer(f1, lineterminator=',')
  no_space = csv.writer(f1, lineterminator='')
  
  comma.writerow([ image_name ])
  if is_right_hand:
    comma.writerow([ 'right' ])
  else:
    comma.writerow([ 'left' ])
    
  comma.writerow([ gesture_name ])
  
  for i in range(21):
    comma.writerow([ handKeypoints[i][0] ]) #write x coordinate
    comma.writerow([ handKeypoints[i][1] ]) #write y coordinate
  
  no_space.writerow('\n')
  f1.close()

def write_handKeypoint_header(csv_filepath):

  header = [
    'center',
    'thumb1',
    'thumb2',
    'thumb3',
    'thumb4',
    'forefinger1',
    'forefinger2',
    'forefinger3',
    'forefinger4',
    'middle_finger1',
    'middle_finger2',
    'middle_finger3',
    'middle_finger4',
    'ring_finder1',
    'ring_finder2',
    'ring_finder3',
    'ring_finder4',
    'pinkie1',
    'pinkie2',
    'pinkie3',
    'pinkie4',
  ]

  f = open(csv_filepath, 'w')
  comma = csv.writer(f, lineterminator=',')
  no_space = csv.writer(f, lineterminator='')
  comma.writerow([ 'image name' ])
  comma.writerow([ 'left or right' ])
  comma.writerow([ 'hand name' ])
  for i in range(len(header)):
    comma.writerow([ header[i] + '_x' ])
    comma.writerow([ header[i] + '_y' ])

  no_space.writerow('\n')
  f.close()

def main():
  args = parse_args()  

  categories = [ 'train', 'test', 'valid' ]
  ratios = [0.5, 0.2, 0.3]

  if not is_video(args.input_path):
    print('Error: Invalid file format, input must be a .mp4, .mov or .avi video.')
    return

  if not os.path.isfile(args.input_path):
    print('Error: Input file does not exist')
    return
    
  for category in categories:
    path = args.output_path + '/' + category + '/' + args.gesture_name + '/'
    if not os.path.exists(path):
        print('Creating directory ',path)
        os.makedirs(path)
    else:
        print('Directory ',path,' was found. Will add new samples to this directory.')
        
  csv_filenames = ['train.csv',
                   'test.csv',
                   'valid.csv']

  for csv_filename in csv_filenames:
    csv_file_path = args.output_path + '/' + csv_filename
    if not os.path.isfile(csv_file_path): #if file doesn't exist create it and write the header
        print('Creating csv file ', csv_file_path)
        write_handKeypoint_header(csv_file_path)
    else: 
        print('File ',csv_file_path,' was found. Will add new samples to this file.')

  random.seed(1) #for reproducible results      
  directory_placer = placer(categories, ratios)
  dataset_generator(args.input_path, args.output_path, args.gesture_name, directory_placer, args.max_samples)
  print("Final placement volumes are ", next(directory_placer))

if __name__ == '__main__':
  main()
