from modules.predictor.predictor_trainer import PredictorTrainer, ModelType
import argparse
import warnings

def parse_args():
  parser = argparse.ArgumentParser(description='Predictor training test unit')
  parser.add_argument('--dataset_path', metavar='dataset_path', required=True,
                        help='Path to the dataset directory to train on', type=str, default=None)
                        
  parser.add_argument('--model_type', metavar='model_type', required=False,
                        help='Model to train on RESNET or VGG', type=str, choices=['RESNET','VGG'], default= 'RESNET')
                        
  parser.add_argument('--epochs', metavar='epochs', required=False,
                        help='Number of epochs to train', type=int, default=5)
                        
  parser.add_argument('--save_path', metavar='save_path',
                        help='Path to save the model', type=str, default=None)
  args = parser.parse_args()
        
  return args

def main():
  args = parse_args()
  print('-----------------Settings-----------------')
  for arg in vars(args):
    print(arg, ': ', getattr(args, arg))
  print('------------------------------------------')

  if args.model_type == 'VGG' :
      pt = PredictorTrainer(model_type = ModelType.VGG)
  elif args.model_type == 'RESNET':
      pt = PredictorTrainer(model_type = ModelType.RESNET)
  else:
      print('Error: Invalid model type. Please enter VGG or RESNET')
      return
    
  pt.load_dataset(data_dir = args.dataset_path, batch_size = 4)
  pt.train_model(num_epochs = args.epochs)
  percent_correct = pt.evaluate_model(verbose = 2)

  if args.save_path is not None:
    print('Saving model to',args.save_path)
    with warnings.catch_warnings():
      warnings.simplefilter("ignore")
      pt.save_model(args.save_path)
    print('Model saved.')
        
if __name__ == '__main__':
    main()