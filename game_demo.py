
from modules.detector.op_ssd_detector import OP_SSD_Detector
from modules.detector.op_detector import OP_Detector
import cv2
import time
from modules.utils.utils import *
import imageio
import os
#from modules.predictor.predictor import Predictor
import argparse
import numpy as np
import modules.utils.settings as settings
import pyautogui as pag
import random

# Detector options

def parse_args():
    parser = argparse.ArgumentParser(description='Detector test unit')
    parser.add_argument('--op_only', help='Use openpose only detector', action="store_true")
    parser.add_argument('--draw', metavar='draw_enable', type=int,
                        help='Set to 0 for no drawing, 1 for keypoint drawing only, >=2 for keypoint and other information', default=1)
    args = parser.parse_args()

    return args


cap = None 
det = None
pred = None
classes = None

def newgame(args):
    global cap 
    global det
    global pred
    global classes

    print('Rock, Paper or Scissors?')
    
    cap = cv2.VideoCapture(0)
    if not cap.isOpened():
        print('Error: Cannot open video capture for ' + args.input_path)
        return
        

    frame_num = 0
    t0 = 0
    while cap.isOpened():
        prev_time = t0
        t0 = time.time()
        time_elapsed = t0 - prev_time
        
        ret, frame = cap.read()  # BGR
        frame_num += 1

        t1 = time.time()
        frame, rectangles, handKeypoints= det.detect_hands(frame, draw_enable = args.draw)
        
        t2 = time.time()

        black_frame = np.zeros( frame.shape, dtype = np.uint8 )
        draw_hand_keypoints(black_frame, rectangles, handKeypoints)
        
        predictions = []
        for i in range(len(rectangles)):  #For each rectangle pair (person)
            for j in range(len(rectangles[i])):  #Left hand then right hand
                if check_data(rectangles[i][j], handKeypoints[j][i]):  #Sanity check
                    x = int(rectangles[i][j].x)
                    y = int(rectangles[i][j].y)
                    w = int(rectangles[i][j].width)
                    h = int(rectangles[i][j].height)
                    cropped_frame = black_frame[max(y, 0):y+h, max(x, 0):x+h]
                    cropped_frame = cv2.resize(cropped_frame,(256,256))
                    prediction, score = pred.predict_image(cropped_frame)
                    if score >= settings.prediction_thresholds[prediction]:
                        predictions.append((classes[prediction], "{0:.2f}".format(score)))

        t3 = time.time()

        if len(predictions) > 0:
            computer_pick = classes[random.randint(0,2)]
            comp_pic = cv2.imread('images/' + 'comp_' + computer_pick+ '.jpg')
            height, width = frame.shape[:2]
            comp_pic = cv2.resize(comp_pic, (width,height))
            frame = cv2.flip(frame, 1)
            black_frame = cv2.flip(black_frame, 1)
            
            title = "Game Results"
            for i in range(0,3):
                for j in range(0,3):
                    if classes[i] == predictions[0][0] and classes[j] == computer_pick:
                        if i == j:
                            title = "Draw"
                        elif j == (i+1)%3:
                            title = "You lose"
                        else:
                            title = "You win"
                        break
                          
            draw_str(frame, (20, 20), "You picked " +  predictions[0][0])
            draw_str(comp_pic, (20, 20), "Computer picked " + computer_pick)
            
            numpy_horizontal = np.hstack( (frame, comp_pic) )
            
            cv2.imshow(title, numpy_horizontal)
            
            while True:
                k = cv2.waitKey(1)
            
                if k == 27 or frame is None:
                    break
            break
            
    if(cap != None):
        cap.release()
        print('Cap has been released')
    cv2.destroyAllWindows()

def main():
    global cap 
    global det
    global pred
    global classes
    
    args = parse_args()
    print('-----------------Settings-----------------')
    for arg in vars(args):
        print(arg, ': ', getattr(args, arg))
    print('------------------------------------------')
    
    if args.op_only:
        print('OP only detector')
        det = OP_Detector()
    else:
        print('OP_SSD detector')
        det = OP_SSD_Detector()

    classes = ['rock','paper','scissor'] #rename fist peace and palm classes
    pred = Predictor(model_path = settings.model_path)
    
    


    playagain = True
    while playagain:
        newgame(args)
        print('Do you want to play again? [y/n]')
        playagain = True if input() in ['y','Y'] else False

if __name__ == '__main__':
    main()
