#!/bin/bash
install_location="./frameworks"

echo 'Preparing directory ...'
mkdir -p $install_location
rm -rf $install_location/ssd

echo 'Cloning github repository ...'
git clone -q --depth 1 https://github.com/victordibia/handtracking.git $install_location/ssd

echo 'Modifying files ...'

sed -i 's/from utils import label_map_util/from . import label_map_util/g' $install_location/ssd/utils/detector_utils.py
sed -i "s#MODEL_NAME = 'hand_inference_graph'#MODEL_NAME = 'frameworks/ssd/hand_inference_graph'#g" $install_location/ssd/utils/detector_utils.py
sed -i 's/from protos import string_int_label_map_pb2/from ..protos import string_int_label_map_pb2/g' $install_location/ssd/utils/label_map_util.py