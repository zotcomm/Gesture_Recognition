from modules.detector.op_ssd_detector import OP_SSD_Detector
from modules.detector.op_detector import OP_Detector
#from modules.predictor.predictor import Predictor
import cv2
import time
from modules.utils.utils import *
import imageio
import os
import argparse
import numpy as np
import modules.utils.settings as settings

def parse_args():
    parser = argparse.ArgumentParser(description='Detector test unit')
    parser.add_argument('--op_only', help='Use openpose only detector', action="store_true")
    parser.add_argument('--input_path', metavar='input_path', required=True,
                        help='Path to the video to use as input', type=str, default=None)
    parser.add_argument('--no_cv_output', help='Prevents opencv output when provided', action="store_true")
    parser.add_argument('--output_path', metavar='output_path',
                        help='Path to output file', type=str, default=None)
    args = parser.parse_args()
        
    return args

def main():

    args = parse_args()
    print('-----------------Settings-----------------')
    for arg in vars(args):
        print(arg, ': ', getattr(args, arg))
    print('------------------------------------------')

    if args.op_only:
        print('OP only detector')
        det = OP_Detector()
    else:
        print('OP_SSD detector')
        det = OP_SSD_Detector()
 
    classes = sorted(settings.classes)  #ensure the classes are sorted
    pred = Predictor(model_path = settings.model_path)
    
    cap = None

    if is_video(args.input_path):
        cap = cv2.VideoCapture(args.input_path)
    else:
        print('Error: Invalid file format, only .mp4, .mov and .avi videos are supported.')
        return

    if not cap.isOpened():
        print('Error: Cannot open video capture for ' + args.input_path)
        return

    out = None
    if args.output_path:
      if is_video(args.output_path):
        if not os.path.dirname(args.output_path) == '':
          os.makedirs(os.path.dirname(args.output_path), exist_ok=True)
          
        out = write_video(cap, args.output_path)
      else:
        print('Error: Output must be an .mp4, .avi or .mov file')
        return
    else:
      print('Only showing output image using opencv without saaving. Please enter an output path if you would like to save it in a file.')

    frame_num = 0
    t0 = 0
    while cap.isOpened():
        prev_time = t0
        t0 = time.time()
        time_elapsed = t0 - prev_time
        ret, frame = cap.read()  # BGR
        frame_num += 1

        k = cv2.waitKey(1)
        if k == 27 or frame is None:
            break

        t1 = time.time()
        frame, rectangles, handKeypoints= det.detect_hands(frame, draw_enable = 2)

        t2 = time.time()
        print('The time for detection on frame ', frame_num, ' : ', t2-t1, ' [sec]')

        black_frame = np.zeros( frame.shape, dtype = np.uint8 )
        draw_hand_keypoints(black_frame, rectangles, handKeypoints)
        
        predictions = []
        for i in range(len(rectangles)):  #For each rectangle pair (person)
            for j in range(len(rectangles[i])):  #Left hand then right hand
                if check_data(rectangles[i][j], handKeypoints[j][i]):  #Sanity check
                    x = int(rectangles[i][j].x)
                    y = int(rectangles[i][j].y)
                    w = int(rectangles[i][j].width)
                    h = int(rectangles[i][j].height)
                    cropped_frame = black_frame[max(y, 0):y+h, max(x, 0):x+h]
                    cropped_frame = cv2.resize(cropped_frame,(256,256))
                    prediction, score = pred.predict_image(cropped_frame)
                    predictions.append((classes[prediction], "{0:.2f}".format(score)))
        
                  
        t3 = time.time()
        print('The time for prediction on frame ', frame_num, ' : ', t3-t2, ' [sec]')
        print('Predictions on frame ', frame_num, ' are : ', predictions)
        
        draw_str(black_frame, (20, 20), "Fps : {:2f}".format( 1/time_elapsed))
        draw_str(black_frame, (20, 40), "Frame {}: {}".format(frame_num,  predictions))
        draw_str(black_frame, (20, 60), "Time to get: {:.3f} sec".format(t1-t0))
        draw_str(black_frame, (20, 80), "Time to detect: {:.3f} sec".format(t2-t1))
        draw_str(black_frame, (20, 100), "Time to predict: {:.3f} sec".format(t3-t2))

        draw_str(frame, (20, 20), "Fps : {:2f}".format( 1/time_elapsed))
        draw_str(frame, (20, 40), "Frame {}: {}".format(frame_num,  predictions))
        draw_str(frame, (20, 60), "Time to get: {:.3f} sec".format(t1-t0))
        draw_str(frame, (20, 80), "Time to detect: {:.3f} sec".format(t2-t1))
        draw_str(frame, (20, 100), "Time to predict: {:.3f} sec".format(t3-t2))

        if args.output_path:
            out.write(frame)
        
        if not args.no_cv_output:
            cv2.imshow('output', frame)
            cv2.imshow('output2', black_frame)

    if(cap != None):
        cap.release()
        print('Cap has been released')
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()